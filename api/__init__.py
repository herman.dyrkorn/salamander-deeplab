from flask import Flask
from flask_limiter.util import get_remote_address
from flask_restful import Api
from flask_limiter import Limiter

# flask application setup, with database, JWT, bcrypt and limiter
app = Flask(__name__)
app.config['SECRET_KEY'] = 'randomchangethisshit'
limiter = Limiter(app, key_func=get_remote_address)
api = Api(app)

# import all classes that is used to create endpoints
from api.endpoints.findsalamanderinfo import FindSalamanderInfo

# add the imported classes to a route
api.add_resource(FindSalamanderInfo, "/findSalamanderInfo")


FROM ubuntu:20.04

ENV TZ=Europe/Oslo

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update -y && \
    apt-get install -y python3-pip python3-matplotlib python-dev git libfreetype6-dev libfreetype6 pkg-config

COPY . /app

WORKDIR /app

RUN pip install git+https://github.com/DeepLabCut/DeepLabCut-core@4e57a0c2709f5e6cce9ac7ab798a8a14e508b9cc

RUN pip install -r requirements.txt

EXPOSE 4500

ENTRYPOINT [ "python3" ]

CMD [ "run.py" ]
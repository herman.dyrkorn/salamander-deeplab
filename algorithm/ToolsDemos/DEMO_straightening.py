import algorithm.straighten_with_dlc as straighten
# CROPPING IMAGE
import cv2
import os
import matplotlib.pyplot as plt
from path_constants import abs_path_dlc_config
from path_constants import abs_path_temp_images
import time
def test():
    cur_dir = os.getcwd()
    image_to_predict = "notwork.png"
    print(abs_path_dlc_config)
    print(abs_path_temp_images)
    print(image_to_predict)
    config =  os.path.abspath(abs_path_dlc_config)
    os_directory_path =  os.path.abspath(abs_path_temp_images)
    image_type = image_to_predict[-4:]
    img_directory = os_directory_path + '/' + image_to_predict
    smallpath = os_directory_path + '/' + 'small' + image_to_predict

    print(img_directory)
    # reading image:
    img = cv2.imread(img_directory)
    # You will have to convert the color if you use OpenCV.
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    print("starting in 10s")
    time.sleep(10)
    print("Waking up")
    str_image, img, points, shoulder_points, _ = straighten.straighten(image = img)
    if str_image is not None:
        str_image = cv2.cvtColor(str_image, cv2.COLOR_BGR2RGB)
        cv2.imwrite(abs_path_temp_images + '/' + image_to_predict[0:-4] + '_str.png', str_image)
        # data points
        implot = plt.imshow(img)
        print(points[0])
        # put a blue dot at (10, 20)
        plt.scatter(points[0][0], points[0][1], c='b', s=40, )
        plt.scatter(points[1][0], points[1][1], c='r', s=40, )
        plt.scatter(points[2][0], points[2][1], c='g', s=40, )
        plt.scatter(points[3][0], points[3][1], c='y', s=40, )
        plt.scatter(shoulder_points[0][0], shoulder_points[0][1], c='y', s=40, )
        plt.scatter(shoulder_points[1][0], shoulder_points[1][1], c='y', s=40, )

        plt.savefig(abs_path_temp_images + '/' + image_to_predict[0:-4] + '_points.png')
        print("done in 10 s (so you can CHECK GPU USAGE)")
        time.sleep(10)

from cv2 import cv2
import numpy as np
import algorithm.dsift as dsift


class SalamanderImage:
    descriptors = []
    filename = ''

    def __init__(self, filename):
        self.filename = filename
        self.descriptors = self.calculate_descriptors()

    def calculate_descriptors(self):
        """
        Calculates the descriptors of the member image

        Returns: The calculated descriptors

        """

        image = cv2.imdecode(np.fromfile(self.filename, dtype=np.uint8), cv2.IMREAD_GRAYSCALE)

        if image is None:
            raise FileNotFoundError("Cannot find image file " + self.filename)
        return dsift.compute_descriptors(image)
